/*!
 * Sports Example v0.1.0 (https://bitbucket.org/artstr/sports.git)
 * Copyright 2014-2014 Artana Pty Ltd
 * Licensed under MIT (https://bitbucket.org/artstr/sports/src/master/LICENSE)
 */

var sliderLogic = (function() {
	"use strict";
	
	var xt = {numSliders: 3,
			  sliderIdPrefix: "weight-",
			  sliderLabelClass: "slider-label",
			  valueIdPrefix: "value-",
			  sliderValueClass: "slider-value",
			  low: 0,
			  high: 100,
			  weights: [],
			  redrawFn: function(){} // you will want to set this
			 };
	if (Object.keys) {
		var validXtKeys = Object.keys(xt);
	}
	
	function getSliderValues() {
		// returns the slider values as an array
		var w = [];
		for (var i = 0; i < xt.numSliders; i++) {
			w.push(+document.getElementById(xt.sliderIdPrefix+i).value);
		}
		return w;
	}

	function setSliderValues(w) {
		// sets the sliders and displayed values to the array of values w
		for (var i = 0; i < w.length; i++) {
			document.getElementById(xt.sliderIdPrefix+i).value = w[i];
			document.getElementById(xt.valueIdPrefix+i).innerHTML =  w[i] + "%";
		}
	}

	function makeSliders(elt, names, values, lo, hi) {
		// eg. sliderLogic.makeSliders($('#detail'),['foo','bar'], [60,40], 0, 100);
		// also sets the internal numSliders, weights, low, high values
		function listener(num) {
			// needs to be in a closure so num isn't replaced with numSliders
			return function() {
				sliderLogic.updateValue(num);
			};
		}
		xt.numSliders = names.length;
		xt.weights = values;
		if (values.length!==xt.numSliders) { throw Error("Names and values must have the same length."); }
		xt.low = lo;
		xt.high = hi;
		for (var i = 0; i < xt.numSliders; i++) {
			var div = document.createElement("div");
			var span = document.createElement("span");
			span.className = xt.sliderLabelClass;
			span.appendChild(document.createTextNode(names[i]));
			div.appendChild(span);
			var slider = document.createElement("input");
			slider.id = xt.sliderIdPrefix+i;
			slider.type = 'range';
			slider.min = xt.low;
			slider.max = xt.high;
			slider.value = values[i];
			slider.addEventListener('input',listener(i));
			div.appendChild(slider);
			var span2 = document.createElement("span");
			span2.className = xt.sliderValueClass;
			span2.id = xt.valueIdPrefix+i;
			div.appendChild(span2);
			elt.appendChild(div);
		}
		setSliderValues(xt.weights);
	}

	function changeOneWeightMaintainingTotal(w, num, newValue) {
		// makes sure the total is unchanged, eg. ([20,30,50], 2, 75) -> [-5,30,75]
		// this does not worry about allowed domain of individual values.
		var diff = (+newValue - w[num]);
		w[num] = +newValue;
		w[(num+1)%w.length] -= diff;
		return w;
	}

	function snapToDomain(w) {
		var carry = 0;
		function snapOneToDomain(i) {
			w[i] += carry;
			carry = 0;
			if (w[i]<xt.low) {
				carry = w[i] - xt.low;
				w[i] = xt.low;
			} else if (w[i]>xt.high) {
				carry = w[i] - xt.high;
				w[i] = xt.high;
			}
		}
		// run through twice
		for (var i = 0; i < w.length; i++) {
			snapOneToDomain(i);
		}
		for (i = 0; i < w.length; i++) {
			snapOneToDomain(i);
		}
		snapOneToDomain(0); // just to be safe
		return w;
	}

	function changeOneWeightMaintainingTotalAndBounds(w, num, newValue) {
		// makes sure the total is unchanged and domain bounds respected,
		// eg. ([20,30,50], 2, 75, 0, 100) -> [0,25,75]
		var w2 = changeOneWeightMaintainingTotal(w, num, newValue);
		var w3 = snapToDomain(w2, xt.low, xt.high);
		return w3;
	}

	function updateValue(num) {
		var newValue = +document.getElementById(xt.sliderIdPrefix+num).value;
		xt.weights = changeOneWeightMaintainingTotalAndBounds(xt.weights, num, newValue, 0, 100);
		setSliderValues(xt.weights);
		xt.redrawFn();
	}

	return {
		makeSliders: makeSliders,
		getSliderValues: getSliderValues,
		setSliderValues: setSliderValues,
		updateValue: updateValue,

		// getter/setter functions
		get: function(name) {
			if (!arguments.length) return xt;
			return xt[name];
		},
		set: function(name, val) {
			if (validXtKeys && validXtKeys.indexOf) {
				if (validXtKeys.indexOf(name)>=0) {
					xt[name] = val;
				} else {
					throw Error("Variable "+name+" not found");
				}
			} else {
				// on browsers without Object.keys and/or indexOf, don't check the name is valid
				xt[name] = val;
			}
			return this;
		}
	};

}() );

//
// Code by Arthur Street
// Artana Pty Ltd
// September 2014
// 
/* global d3: true */
if (typeof d3 === 'undefined') { throw new Error('This module requires d3') }
//if (typeof jQuery === 'undefined') { throw new Error('This module requires jQuery') }

var d3 = (function (d3) {
	"use strict";
	// requires d3
	// adds d3.reusable.makeHoverPanel

	var makeHoverPanel = function(selector) {
		// Make the hover element if needed, in the supplied element selector (defaults to "body")
		// Returns a function which other widgets can use to show & hide html in a hover box
		var hoverDiv = null,
			hoverOffset = {top: 20, left: 11, bottom: 20},
			makeHidden = false,
			opacity = 1;

		function showOrHideHoverPanel (elt, show) {  // ,d
			// in this implementation, elt is not used
			//console.log(elt);
			if (show) {
				hoverDiv
					//.html(html)
					.style("top", function () { 
						return Math.min((d3.event.pageY - hoverOffset.top), Math.max(5,window.innerHeight-hoverOffset.bottom-hoverDiv[0][0].offsetHeight))+"px";
					})
					.style("left", function () { 
						if (d3.event.pageX > (window.innerWidth/2)) {
							return (d3.event.pageX - hoverOffset.left - hoverDiv[0][0].offsetWidth)+"px";
						} else {
							return (d3.event.pageX + hoverOffset.left)+"px";
						}
					}) 
					.style("visibility", "visible")
					.style("z-index", "5")
					.style("opacity", 1e-6)
					.transition()
						.style("opacity", opacity);
			} else {
				// hide
				hoverDiv
					.transition()
						.style("opacity", 1e-6)
						.style("z-index", "-1")
						.style("visibility", makeHidden ? "hidden" : "visible");
			}
		}

		showOrHideHoverPanel.show = function(svg, d) { showOrHideHoverPanel(svg, true, d); return this; };
		showOrHideHoverPanel.hide = function(svg, d) { showOrHideHoverPanel(svg, false, d); return this; };

		// Make the hover element if needed
		if (typeof selector === "undefined") { selector = "body"; }
		hoverDiv = d3.select(selector).selectAll("div.hover").data(["TBD"]);
		hoverDiv.enter().append("div").attr("class","hover");

		showOrHideHoverPanel.hoverOffset = function(_) {
			if (!arguments.length) return hoverOffset;
			hoverOffset = _;
			return showOrHideHoverPanel;
		};

		showOrHideHoverPanel.makeHidden = function(_) {
			if (!arguments.length) return makeHidden;
			makeHidden = _;
			return showOrHideHoverPanel;
		};

		showOrHideHoverPanel.opacity = function(_) {
			if (!arguments.length) return opacity;
			opacity = _;
			return showOrHideHoverPanel;
		};

		return showOrHideHoverPanel;

	};
	// attach to d3.reusable
	if (typeof d3.reusable==="undefined") {
		d3.reusable = {};
	}
	d3.reusable.makeHoverPanel = makeHoverPanel;
	return d3;

}(d3));

//
// Code by Arthur Street
// Artana Pty Ltd
// December 2014
// 
/* global d3, sliderLogic */

(function(d3) {
	"use strict";

	var elt = d3.select("#detail");
	var width = 660, 
		height = 450;
	var cats = ['NSW','Vic','SA','Tas','WA','Qld','ACT','NT','NZ'];
	var boys=[], girls=[], robots=[];
	var points = [];

	var sliderNames = ['School Entry Level', 'School Traditional', 'Other'];
	var sliderInitVals = [20, 5, 75];

	var hoverWidget = d3.reusable.makeHoverPanel(".hover-panel").makeHidden(true);

	var myChart;

	function redrawChart() {
		var weights = sliderLogic.get('weights');
		hoverWidget.hide();
		points.forEach(function(pt, i) {
			pt[1] = boys[i]*weights[0]/100+girls[i]*weights[1]/100+robots[i]*weights[2]/100;
		});
		//console.log(points);  // e.g. [['Beagle',15.5], ['Poodle', -33.1], ...]
		elt.datum(points).call(myChart.duration(0));
	}

	sliderLogic.set("redrawFn",redrawChart).makeSliders(document.getElementById("sliders"),sliderNames, sliderInitVals, 0, 100);
	// var weights = sliderLogic.getSliderValues();
	// sliderLogic.setSliderValues(weights);
	var weights = sliderLogic.get('weights');

	cats.forEach(function(cat) {
		boys.push(Math.random()*50-20);
		girls.push(Math.random()*50-20);
		robots.push(Math.random()*50-30);
		points.push([cat, boys[boys.length-1]*weights[0]/100+girls[girls.length-1]*weights[1]/100+robots[robots.length-1]*weights[2]/100]);
	});

	function addChartToHover(d) {
		var n = cats.indexOf(d[0]);
		var hoverData = [['Boys',boys[n]], ['Girls',girls[n]], ['Robots',robots[n]]];
		var hoverChart = d3.elts.barChart()
							.width(250)
							.height(height/2)
							.yMin(-30).yMax(30)
							.margin({top: 10, right: 10, bottom: 35, left: 40})
							.yValueOfXAxis(-30)
							.xAxisAnimate(false)
							.xPadding(0.2)
							.fill('lightgray').stroke('gray')
							.xAxisText(function(textSel){textSel.attr("dy", ".35em")});
		d3.select('.hover').datum(hoverData).call(hoverChart);
	}

	var yMin =  function(data) { return Math.min(-20, d3.min(data, function(d) {return d[1]})) };
	var yMax =  function(data) { return Math.max(30, d3.max(data, function(d) {return d[1]})) };

	myChart = d3.elts.barChart()
						.width(width)
						.height(height)
						.mouseOver(function(elt, d) {addChartToHover(d); hoverWidget.show(elt, d);})
						.yMin(yMin)
						.yMax(yMax)
						.margin({top: 20, right: 20, bottom: 45, left: 50})
						.yValueOfXAxis(yMin)
						.xAxisAnimate(false)
						.xAxisText(function(textSel){textSel.attr("dy", ".65em")});

	elt.datum(points).call(myChart);


}(d3));
