var sliderLogic = (function() {
	"use strict";
	
	var xt = {numSliders: 3,
			  sliderIdPrefix: "weight-",
			  sliderLabelClass: "slider-label",
			  valueIdPrefix: "value-",
			  sliderValueClass: "slider-value",
			  low: 0,
			  high: 100,
			  weights: [],
			  redrawFn: function(){} // you will want to set this
			 };
	if (Object.keys) {
		var validXtKeys = Object.keys(xt);
	}
	
	function getSliderValues() {
		// returns the slider values as an array
		var w = [];
		for (var i = 0; i < xt.numSliders; i++) {
			w.push(+document.getElementById(xt.sliderIdPrefix+i).value);
		}
		return w;
	}

	function setSliderValues(w) {
		// sets the sliders and displayed values to the array of values w
		for (var i = 0; i < w.length; i++) {
			document.getElementById(xt.sliderIdPrefix+i).value = w[i];
			document.getElementById(xt.valueIdPrefix+i).innerHTML =  w[i] + "%";
		}
	}

	function makeSliders(elt, names, values, lo, hi) {
		// eg. sliderLogic.makeSliders($('#detail'),['foo','bar'], [60,40], 0, 100);
		// also sets the internal numSliders, weights, low, high values
		function listener(num) {
			// needs to be in a closure so num isn't replaced with numSliders
			return function() {
				sliderLogic.updateValue(num);
			};
		}
		xt.numSliders = names.length;
		xt.weights = values;
		if (values.length!==xt.numSliders) { throw Error("Names and values must have the same length."); }
		xt.low = lo;
		xt.high = hi;
		for (var i = 0; i < xt.numSliders; i++) {
			var div = document.createElement("div");
			var span = document.createElement("span");
			span.className = xt.sliderLabelClass;
			span.appendChild(document.createTextNode(names[i]));
			div.appendChild(span);
			var slider = document.createElement("input");
			slider.id = xt.sliderIdPrefix+i;
			slider.type = 'range';
			slider.min = xt.low;
			slider.max = xt.high;
			slider.value = values[i];
			slider.addEventListener('input',listener(i));
			div.appendChild(slider);
			var span2 = document.createElement("span");
			span2.className = xt.sliderValueClass;
			span2.id = xt.valueIdPrefix+i;
			div.appendChild(span2);
			elt.appendChild(div);
		}
		setSliderValues(xt.weights);
	}

	function changeOneWeightMaintainingTotal(w, num, newValue) {
		// makes sure the total is unchanged, eg. ([20,30,50], 2, 75) -> [-5,30,75]
		// this does not worry about allowed domain of individual values.
		var diff = (+newValue - w[num]);
		w[num] = +newValue;
		w[(num+1)%w.length] -= diff;
		return w;
	}

	function snapToDomain(w) {
		var carry = 0;
		function snapOneToDomain(i) {
			w[i] += carry;
			carry = 0;
			if (w[i]<xt.low) {
				carry = w[i] - xt.low;
				w[i] = xt.low;
			} else if (w[i]>xt.high) {
				carry = w[i] - xt.high;
				w[i] = xt.high;
			}
		}
		// run through twice
		for (var i = 0; i < w.length; i++) {
			snapOneToDomain(i);
		}
		for (i = 0; i < w.length; i++) {
			snapOneToDomain(i);
		}
		snapOneToDomain(0); // just to be safe
		return w;
	}

	function changeOneWeightMaintainingTotalAndBounds(w, num, newValue) {
		// makes sure the total is unchanged and domain bounds respected,
		// eg. ([20,30,50], 2, 75, 0, 100) -> [0,25,75]
		var w2 = changeOneWeightMaintainingTotal(w, num, newValue);
		var w3 = snapToDomain(w2, xt.low, xt.high);
		return w3;
	}

	function updateValue(num) {
		var newValue = +document.getElementById(xt.sliderIdPrefix+num).value;
		xt.weights = changeOneWeightMaintainingTotalAndBounds(xt.weights, num, newValue, 0, 100);
		setSliderValues(xt.weights);
		xt.redrawFn();
	}

	return {
		makeSliders: makeSliders,
		getSliderValues: getSliderValues,
		setSliderValues: setSliderValues,
		updateValue: updateValue,

		// getter/setter functions
		get: function(name) {
			if (!arguments.length) return xt;
			return xt[name];
		},
		set: function(name, val) {
			if (typeof validXtKeys!=="undefined" && validXtKeys.indexOf) {
				if (validXtKeys.indexOf(name)>=0) {
					xt[name] = val;
				} else {
					throw Error("Variable "+name+" not found");
				}
			} else {
				// on browsers without Object.keys and/or indexOf, don't check the name is valid
				xt[name] = val;
			}
			return this;
		}
	};

}() );
