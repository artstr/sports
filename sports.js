//
// Code by Arthur Street
// Artana Pty Ltd
// December 2014
// 
/* global d3, sliderLogic */

(function(d3) {
	"use strict";

	var elt = d3.select("#detail");
	var width = 660, 
		height = 450;
	var cats = ['NSW','Vic','SA','Tas','WA','Qld','ACT','NT','NZ'];
	var boys=[], girls=[], robots=[];
	var points = [];

	var sliderNames = ['School Entry Level', 'School Traditional', 'Other'];
	var sliderInitVals = [20, 5, 75];

	var hoverWidget = d3.reusable.makeHoverPanel(".hover-panel").makeHidden(true);

	var myChart;

	function redrawChart() {
		var weights = sliderLogic.get('weights');
		hoverWidget.hide();
		points.forEach(function(pt, i) {
			pt[1] = boys[i]*weights[0]/100+girls[i]*weights[1]/100+robots[i]*weights[2]/100;
		});
		//console.log(points);  // e.g. [['Beagle',15.5], ['Poodle', -33.1], ...]
		elt.datum(points).call(myChart.duration(0));
	}

	sliderLogic.set("redrawFn",redrawChart).makeSliders(document.getElementById("sliders"),sliderNames, sliderInitVals, 0, 100);
	// var weights = sliderLogic.getSliderValues();
	// sliderLogic.setSliderValues(weights);
	var weights = sliderLogic.get('weights');

	cats.forEach(function(cat) {
		boys.push(Math.random()*50-20);
		girls.push(Math.random()*50-20);
		robots.push(Math.random()*50-30);
		points.push([cat, boys[boys.length-1]*weights[0]/100+girls[girls.length-1]*weights[1]/100+robots[robots.length-1]*weights[2]/100]);
	});

	function addChartToHover(d) {
		var n = cats.indexOf(d[0]);
		var hoverData = [['Boys',boys[n]], ['Girls',girls[n]], ['Robots',robots[n]]];
		var hoverChart = d3.elts.barChart()
							.width(250)
							.height(height/2)
							.yMin(-30).yMax(30)
							.margin({top: 10, right: 10, bottom: 35, left: 40})
							.yValueOfXAxis(-30)
							.xAxisAnimate(false)
							.xPadding(0.2)
							.fill('lightgray').stroke('gray')
							.xAxisText(function(textSel){textSel.attr("dy", ".35em")});
		d3.select('.hover').datum(hoverData).call(hoverChart);
	}

	var yMin =  function(data) { return Math.min(-20, d3.min(data, function(d) {return d[1]})) };
	var yMax =  function(data) { return Math.max(30, d3.max(data, function(d) {return d[1]})) };

	myChart = d3.elts.barChart()
						.width(width)
						.height(height)
						.mouseOver(function(elt, d) {addChartToHover(d); hoverWidget.show(elt, d);})
						.yMin(yMin)
						.yMax(yMax)
						.margin({top: 20, right: 20, bottom: 45, left: 50})
						.yValueOfXAxis(yMin)
						.xAxisAnimate(false)
						.xAxisText(function(textSel){textSel.attr("dy", ".65em")});

	elt.datum(points).call(myChart);


}(d3));
