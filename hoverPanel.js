//
// Code by Arthur Street
// Artana Pty Ltd
// September 2014
// 
/* global d3: true */
if (typeof d3 === 'undefined') { throw new Error('This module requires d3') }
//if (typeof jQuery === 'undefined') { throw new Error('This module requires jQuery') }

var d3 = (function (d3) {
	"use strict";
	// requires d3
	// adds d3.reusable.makeHoverPanel

	var makeHoverPanel = function(selector) {
		// Make the hover element if needed, in the supplied element selector (defaults to "body")
		// Returns a function which other widgets can use to show & hide html in a hover box
		var hoverDiv = null,
			hoverOffset = {top: 20, left: 11, bottom: 20},
			makeHidden = false,
			opacity = 1;

		function showOrHideHoverPanel (elt, show) {  // ,d
			// in this implementation, elt is not used
			//console.log(elt);
			if (show) {
				hoverDiv
					//.html(html)
					.style("top", function () { 
						return Math.min((d3.event.pageY - hoverOffset.top), Math.max(5,window.innerHeight-hoverOffset.bottom-hoverDiv[0][0].offsetHeight))+"px";
					})
					.style("left", function () { 
						if (d3.event.pageX > (window.innerWidth/2)) {
							return (d3.event.pageX - hoverOffset.left - hoverDiv[0][0].offsetWidth)+"px";
						} else {
							return (d3.event.pageX + hoverOffset.left)+"px";
						}
					}) 
					.style("visibility", "visible")
					.style("z-index", "5")
					.style("opacity", 1e-6)
					.transition()
						.style("opacity", opacity);
			} else {
				// hide
				hoverDiv
					.transition()
						.style("opacity", 1e-6)
						.style("z-index", "-1")
						.style("visibility", makeHidden ? "hidden" : "visible");
			}
		}

		showOrHideHoverPanel.show = function(svg, d) { showOrHideHoverPanel(svg, true, d); return this; };
		showOrHideHoverPanel.hide = function(svg, d) { showOrHideHoverPanel(svg, false, d); return this; };

		// Make the hover element if needed
		if (typeof selector === "undefined") { selector = "body"; }
		hoverDiv = d3.select(selector).selectAll("div.hover").data(["TBD"]);
		hoverDiv.enter().append("div").attr("class","hover");

		showOrHideHoverPanel.hoverOffset = function(_) {
			if (!arguments.length) return hoverOffset;
			hoverOffset = _;
			return showOrHideHoverPanel;
		};

		showOrHideHoverPanel.makeHidden = function(_) {
			if (!arguments.length) return makeHidden;
			makeHidden = _;
			return showOrHideHoverPanel;
		};

		showOrHideHoverPanel.opacity = function(_) {
			if (!arguments.length) return opacity;
			opacity = _;
			return showOrHideHoverPanel;
		};

		return showOrHideHoverPanel;

	};
	// attach to d3.reusable
	if (typeof d3.reusable==="undefined") {
		d3.reusable = {};
	}
	d3.reusable.makeHoverPanel = makeHoverPanel;
	return d3;

}(d3));
